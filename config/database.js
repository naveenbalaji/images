const mongoose=require('mongoose');

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true,useFindAndModify: false },(err)=>{
if(err) 
{console.log(err);}
else{
    console.log("Mongoose connected ");
}

})

module.exports=mongoose;


// const MongoClient = require('mongodb').MongoClient;
// const client = new MongoClient(process.env.MONGO_URI , { useNewUrlParser: true ,useUnifiedTopology: true});
// client.connect(err => {
//     if(err) console.log(err)
//     console.log("mongodb Connected");
// });

// module.exports=client;
